#!/bin/sh
# Test if the migrations are up-to-date for tags, and otherwise, make them
export RUNNING_TESTS=1

set -e

case $CI_COMMIT_TAG in
    "")
        python manage.py makemigrations;;
    *)
        python manage.py makemigrations;;
esac
